import {View, SafeAreaView, FlatList} from 'react-native';
import React from 'react';
import {useTailwind} from 'tailwind-rn';

import DATA from '../utils/profileData';
import happyFace from '../assets/HappyFace.png';
import ProfileHeader from '../components/Profile/ProfileHeader';
import Referral from '../components/Profile/Referral';
import ProfileList from '../components/Profile/ProfileList';

const ProfileScreen = () => {
  const tw = useTailwind();

  const renderHeader = () => (
    <View style={tw('p-6')}>
      <ProfileHeader
        name={DATA.name}
        member={DATA.member}
        orders={DATA.orders}
      />
      <Referral image={happyFace} />
    </View>
  );

  const renderItem = ({item, i}) => {
    return <ProfileList key={i} icon={item.icon} name={item.name} />;
  };

  return (
    <SafeAreaView style={tw('flex flex-1 bg-white pb-44')}>
      <FlatList
        contentContainerStyle={tw('flex flex-1 items-center px-4 pb-56')}
        ListHeaderComponent={renderHeader}
        showsVerticalScrollIndicator={false}
        initialNumToRender={5}
        data={DATA.data ? DATA.data : []}
        renderItem={renderItem}
        keyExtractor={(item, index) => index}
        onEndReachedThreshold={0.2}
      />
    </SafeAreaView>
  );
};

export default ProfileScreen;
