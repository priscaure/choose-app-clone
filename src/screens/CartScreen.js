import {View} from 'react-native';
import React from 'react';
import {useTailwind} from 'tailwind-rn';
import Title from '../components/Title';

import Button from '../components/Button';
import ChooseLogo from '../components/ChooseLogo';

const CartScreen = ({navigation}) => {
  const tw = useTailwind();
  return (
    <View
      style={tw('flex flex-1 bg-choosePrimary justify-center items-center')}>
      <ChooseLogo style={tw('h-12 w-full mb-6')} />
      <Title title="Your cart is empty" />
      <Button title="Shop Now" onPress={() => navigation.navigate('Home')} />
    </View>
  );
};

export default CartScreen;
