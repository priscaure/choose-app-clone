import React from 'react';
import {View, FlatList} from 'react-native';
import {PRODUCTS_QUERY, PRODUCT_QUERY} from '../graphql/products';
import {useTailwind} from 'tailwind-rn';
import {useQuery} from '@apollo/client';

import Empty from '../components/Empty';
import ProductPlaceholder from '../components/Product/ProductPlaceholder';
import {Notifier, NotifierComponents} from 'react-native-notifier';
import ProductHeader from '../components/Product/ProductHeader';

const ProductScreen = ({route}) => {
  const tw = useTailwind();
  const {productId} = route.params;
  const {loading: productsLoading, data} = useQuery(PRODUCTS_QUERY, {
    variables: {limit: 5},
  });

  const {
    loading,
    error,
    data: product,
  } = useQuery(PRODUCT_QUERY, {
    fetchPolicy: 'network-only',
    variables: {id: productId},
  });

  const renderItem = () => <ProductPlaceholder />;

  const renderHeader = () => {
    return (
      <ProductHeader
        product={product.product}
        disabled
        numberOfProducts={data.products.length}
      />
    );
  };

  if (loading || productsLoading) {
    return <Empty loading={loading} loadingText="Product loading..." />;
  }

  if (error) {
    return Notifier.showNotification({
      title: 'Error',
      description: error.message,
      duration: 0,
      showAnimationDuration: 800,
      Component: NotifierComponents.Alert,
      componentProps: {
        alertType: 'error',
        backgroundColor: '#091116',
      },
    });
  }

  return (
    <View style={tw('flex flex-1 bg-white')}>
      <FlatList
        ListHeaderComponent={renderHeader}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={tw('bg-white')}
        data={data.products}
        numColumns={2}
        columnWrapperStyle={tw('px-2')}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

export default ProductScreen;
