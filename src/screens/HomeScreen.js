import {FlatList, SafeAreaView, RefreshControl} from 'react-native';
import React, {useState, useEffect} from 'react';
import {NetworkStatus, useQuery} from '@apollo/client';
import {useTailwind} from 'tailwind-rn';

import {PRODUCTS_QUERY} from '../graphql/products';
import ProductCard from '../components/Product/ProductCard';

import Empty from '../components/Empty';
import {Notifier, NotifierComponents} from 'react-native-notifier';

const LIMIT = 5;

const HomeScreen = ({navigation}) => {
  const tw = useTailwind();
  const [limit, setLimit] = useState(LIMIT);
  const [feedData, setFeedData] = useState([]);
  const {loading, error, data, refetch, networkStatus, fetchMore} = useQuery(
    PRODUCTS_QUERY,
    {
      fetchPolicy: 'network-only',
      variables: {limit: limit, offset: 0},
    },
  );
  const refreshing = networkStatus === NetworkStatus.refetch;

  useEffect(() => {
    if (data) {
      setFeedData(data.products);
    }
  }, [data]);

  if (error) {
    Notifier.showNotification({
      title: 'Error',
      description: error.message,
      duration: 0,
      showAnimationDuration: 800,
      Component: NotifierComponents.Alert,
      componentProps: {
        alertType: 'error',
        backgroundColor: '#091116',
      },
    });
    return <Empty loading={loading} loadingText="Error... Try again later" />;
  }

  const onEndReached = async () => {
    if (!refreshing && data.products.length >= limit) {
      await fetchMore({
        variables: {
          limit: limit,
        },
      }).then(fetchMoreResult => {
        console.log('fetchMoreResult: ', fetchMoreResult);
        setLimit(feedData.length + fetchMoreResult.data.products.length);
      });
    }
  };

  const renderItem = ({item}) => {
    return (
      <ProductCard
        item={item}
        onPress={() => navigation.navigate('Product', {productId: item.id})}
      />
    );
  };

  const renderEmpty = () => (
    <Empty loading={loading} loadingText="Products loading..." />
  );

  return (
    <SafeAreaView style={tw('bg-choosePrimary flex flex-1')}>
      <FlatList
        showsVerticalScrollIndicator={false}
        initialNumToRender={5}
        contentContainerStyle={tw('bg-white')}
        data={feedData ? feedData : []}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.2}
        ListEmptyComponent={renderEmpty}
        refreshControl={
          <RefreshControl
            tintColor={tw('bg-chooseBlack')}
            refreshing={refreshing}
            onRefresh={() => refetch()}
          />
        }
      />
    </SafeAreaView>
  );
};

export default HomeScreen;
