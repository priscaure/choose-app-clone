import {gql} from '@apollo/client';

export const PRODUCTS_QUERY = gql`
  query Products($limit: Int, $offset: Int) {
    products(limit: $limit, offset: $offset)
      @rest(type: "Products", path: "products?limit={args.limit}&sort=desc") {
      id
      title
      price
      category
      description
      image
    }
  }
`;

export const PRODUCT_QUERY = gql`
  query Product($id: ID!) {
    product(id: $id) @rest(type: "Product", path: "products/{args.id}") {
      id
      title
      price
      category
      description
      image
    }
  }
`;
