import {Text, View} from 'react-native';
import React, {FC} from 'react';

import {HEIGHT} from '../utils/constants';
import ChooseLogo from './ChooseLogo';
import {useTailwind} from 'tailwind-rn';

interface Props {
  loading: boolean;
  loadingText: string;
}

const Empty: FC<Props> = ({loading, loadingText}) => {
  const tw = useTailwind();
  return (
    <View
      style={[
        tw('flex flex-1 justify-center items-center bg-choosePrimary'),
        {
          height: HEIGHT / 1.2,
        },
      ]}>
      <ChooseLogo style={tw('h-12 w-12')} />
      {loading && <Text style={tw('text-center')}>{loadingText}</Text>}
    </View>
  );
};

export default Empty;
