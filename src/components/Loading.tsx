import {View, ActivityIndicator} from 'react-native';
import React from 'react';
import {useTailwind} from 'tailwind-rn';

const Loading = () => {
  const tw = useTailwind();
  return (
    <View
      style={tw('flex flex-1 justify-center items-center bg-choosePrimary')}>
      <ActivityIndicator size="small" color="#091116" />
    </View>
  );
};

export default Loading;
