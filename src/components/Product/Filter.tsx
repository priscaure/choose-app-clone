import { View, Text } from 'react-native'
import React, { FC } from 'react'
import { useTailwind } from 'tailwind-rn/'
import { IProducts } from '../../types/Product'

interface Props {
  numberOfProducts: number
}

const Filter: FC<Props> = ({ numberOfProducts }) => {
  const tw = useTailwind()

  return (
    <View
      style={tw(
        'p-2 mx-2 border-t border-chooseGrey flex flex-row justify-between',
      )}>
      <Text style={tw('text-xs')}>{numberOfProducts} items</Text>
      <Text style={tw('font-bold text-chooseBlack')}>Filter</Text>
    </View>
  )
}

export default Filter