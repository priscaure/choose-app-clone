import React, {FC} from 'react';
import ProductCard from './ProductCard';
import Filter from './Filter';
import {IProduct} from '../../types/Product';

interface Props {
  product: IProduct;
  numberOfProducts: number;
  disabled: boolean;
  onPress(): void;
}

const ProductHeader: FC<Props> = ({
  product,
  numberOfProducts,
  disabled,
  onPress,
}) => {
  return (
    <>
      <ProductCard item={product} disabled={disabled} onPress={onPress} />
      <Filter numberOfProducts={numberOfProducts} />
    </>
  );
};

export default ProductHeader;
