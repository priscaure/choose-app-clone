import { View } from 'react-native'
import React from 'react'
import ChooseLogo from '../ChooseLogo'
import { useTailwind } from 'tailwind-rn'

const ProductPlaceholder = () => {
  const tw = useTailwind()

  return (
    <View
      style={tw(
        'flex flex-1 justify-center items-center h-44 bg-choosePrimary m-2',
      )}>
      <ChooseLogo style={tw('h-8 w-8')} />
    </View>
  )
}

export default ProductPlaceholder