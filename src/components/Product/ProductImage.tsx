import React, { FC } from 'react';
import {Image} from 'react-native';
import {useTailwind} from 'tailwind-rn';

interface Props {
  image: string;
}

const ProductImage: FC<Props> = ({image}) => {
  const tw = useTailwind();
  return (
    <Image
      resizeMode="center"
      source={{uri: image}}
      style={tw('w-full h-52')}
    />
  );
};

export default ProductImage;
