import React, { FC } from 'react';
import {Text} from 'react-native';
import {useTailwind} from 'tailwind-rn';

interface Props {
  category: string;
}

const ProductCategory: FC<Props> = ({category}) => {
  const tw = useTailwind();
  return (
    <Text style={tw('uppercase text-chooseGrey text-xs')}>{category}</Text>
  );
};

export default ProductCategory;
