import {TouchableOpacity, View} from 'react-native';
import React, {FC} from 'react';
import ProductImage from './ProductImage';
import Title from '../Title';
import ProductCategory from './ProductCategory';
import {useTailwind} from 'tailwind-rn';
import {IProduct} from '../../types/Product';

interface Props {
  item: IProduct;
  onPress(): void;
  disabled: boolean;
}

const ProductCard: FC<Props> = ({item, onPress, disabled}) => {
  const tw = useTailwind();
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.8} disabled={disabled}>
      <View style={tw('mb-2')}>
        <ProductImage image={item.image} />
        <View style={tw('p-4')}>
          <Title title={item.title} />
          <ProductCategory category={item.category} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ProductCard;
