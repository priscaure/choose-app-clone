import React, {FC} from 'react';
import {Text} from 'react-native';
import {useTailwind} from 'tailwind-rn';

interface Props {
  title: string;
}

const Title: FC<Props> = ({title}) => {
  const tw = useTailwind();
  return (
    <Text
      style={tw('mb-2 font-customTitle text-lg leading-5 text-chooseBlack')}>
      {title}
    </Text>
  );
};

export default Title;
