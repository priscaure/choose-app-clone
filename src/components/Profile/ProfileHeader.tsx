import React, {FC} from 'react';
import {View, Text} from 'react-native';
import {useTailwind} from 'tailwind-rn';

import ChooseLogo from '../ChooseLogo';
import Title from '../Title';

interface Props {
  name: string;
  member: string;
  orders: number;
}

const ProfileHeader: FC<Props> = ({name, member, orders}) => {
  const tw = useTailwind();
  return (
    <View style={tw('items-center')}>
      <View style={tw('rounded-full border border-chooseBlack p-4')}>
        <ChooseLogo style={tw('h-12 w-12')} />
      </View>
      <View style={tw('mt-4')}>
        <Title title={name} />
      </View>
      <Text style={tw('text-chooseGrey text-xs')}>
        {member} • {orders} orders
      </Text>
    </View>
  );
};

export default ProfileHeader;
