import {View, Text, Image} from 'react-native';
import React, {FC} from 'react';
import {useTailwind} from 'tailwind-rn';
import {ChevronRightIcon} from 'react-native-heroicons/outline';

interface Props {
  image: string;
}

const Referral: FC<Props> = ({image}) => {
  const tw = useTailwind();

  return (
    <View
      style={tw(
        'w-full bg-choosePrimary h-24 mt-6 mb-4 rounded-lg flex flex-row justify-between items-center p-4',
      )}>
      <Image source={image} style={tw('h-12 w-12')} />
      <View>
        <Text style={tw('font-bold text-[16px] mb-2')}>Referral</Text>
        <Text style={tw('text-chooseGrey')}>Invite your friends</Text>
      </View>
      <ChevronRightIcon color="#091116" size={24} style={tw('pr-8')} />
    </View>
  );
};

export default Referral;
