import {View, Text} from 'react-native';
import React, {FC, ReactNode} from 'react';
import {useTailwind} from 'tailwind-rn';
import {WIDTH} from '../../utils/constants';

interface Props {
  icon: ReactNode;
  name: string;
}

const ProfileList: FC<Props> = ({icon, name}) => {
  const tw = useTailwind();
  return (
    <View
      style={[
        tw(
          'flex flex-row items-center h-16 border-b border-b-chooseGrey w-full p-4',
        ),
        {width: WIDTH},
      ]}>
      <View style={tw('mr-2')}>{icon}</View>
      <Text>{name}</Text>
    </View>
  );
};

export default ProfileList;
