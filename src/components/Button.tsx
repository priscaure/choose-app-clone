import {TouchableOpacity, Text} from 'react-native';
import React, { FC } from 'react';
import {useTailwind} from 'tailwind-rn';

interface Props {
  title: string;
  onPress(): void;
}

const Button: FC<Props> = ({title, onPress}) => {
  const tw = useTailwind();
  return (
    <TouchableOpacity
      onPress={onPress}
      style={tw('flex justify-center px-6 h-10 bg-chooseBlack rounded mt-4')}>
      <Text style={tw('font-bold text-white')}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;
