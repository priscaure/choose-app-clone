import React from 'react';
import {
  ArchiveIcon,
  QuestionMarkCircleIcon,
  UserCircleIcon,
  CurrencyEuroIcon,
  BellIcon,
  GiftIcon,
} from 'react-native-heroicons/outline';

export default {
  name: 'Prisca Aure',
  member: 'Proud member since September 2021',
  orders: 3,
  data: [
    {
      icon: <ArchiveIcon size={20} color="#091116" />,
      name: 'My Orders',
    },
    {
      icon: <QuestionMarkCircleIcon size={20} color="#091116" />,
      name: 'Help',
    },
    {
      icon: <UserCircleIcon size={20} color="#091116" />,
      name: 'My Details',
    },
    {
      icon: <CurrencyEuroIcon size={20} color="#091116" />,
      name: 'My Credits',
    },
    {
      icon: <BellIcon size={20} color="#091116" />,
      name: 'Notifications & Emails',
    },
    {
      icon: <GiftIcon size={20} color="#091116" />,
      name: 'Gift Card',
    },
  ],
};
