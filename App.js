/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {RestLink} from 'apollo-link-rest';
import {ApolloClient, InMemoryCache, ApolloProvider} from '@apollo/client';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {StyleSheet, Text, View} from 'react-native';
import {TailwindProvider} from 'tailwind-rn';
import {
  HomeIcon,
  UserIcon,
  ShoppingBagIcon,
} from 'react-native-heroicons/outline';
import {
  HomeIcon as HomeIconSolid,
  UserIcon as UserIconSolid,
  ShoppingBagIcon as ShoppingBagIconSolid,
} from 'react-native-heroicons/solid';
import {NotifierWrapper} from 'react-native-notifier';

import utilities from './tailwind.json';
import HomeScreen from './src/screens/HomeScreen';
import CartScreen from './src/screens/CartScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import ProductScreen from './src/screens/ProductScreen';
import ChooseLogo from './src/components/ChooseLogo';

const restLink = new RestLink({
  uri: 'https://fakestoreapi.com/',
});

const client = new ApolloClient({
  link: restLink,
  cache: new InMemoryCache(),
});

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainStack = () => {
  return (
    <Stack.Navigator
      screenOptions={({}) => ({
        headerBackTitle: '',
        headerTintColor: '#091116',
      })}>
      <Stack.Screen
        name="All Our Sales"
        component={HomeScreen}
        options={{
          headerTitle: () => (
            <View style={styles.header}>
              <ChooseLogo style={styles.logo} />
              <Text style={styles.headerText}>All Our Sales</Text>
            </View>
          ),
        }}
      />
      <Stack.Screen
        name="Product"
        component={ProductScreen}
        options={{
          headerTitle: () => <ChooseLogo style={styles.headerLogo} />,
          headerShadowVisible: false,
        }}
      />
    </Stack.Navigator>
  );
};

const App = () => {
  return (
    <NotifierWrapper>
      <ApolloProvider client={client}>
        <TailwindProvider utilities={utilities}>
          <NavigationContainer>
            <Tab.Navigator
              screenOptions={({}) => ({
                tabBarShowLabel: false,
              })}>
              <Tab.Screen
                name="Home"
                component={MainStack}
                options={{
                  headerShown: false,
                  tabBarIcon: ({focused}) =>
                    focused ? (
                      <HomeIconSolid size={24} color="#091116" />
                    ) : (
                      <HomeIcon size={24} color="#8a8d8f" />
                    ),
                }}
              />
              <Tab.Screen
                name="My Cart"
                component={CartScreen}
                options={{
                  tabBarIcon: ({focused}) =>
                    focused ? (
                      <ShoppingBagIconSolid size={24} color="#091116" />
                    ) : (
                      <ShoppingBagIcon size={24} color="#8a8d8f" />
                    ),
                }}
              />
              <Tab.Screen
                name="My Account"
                component={ProfileScreen}
                options={{
                  tabBarIcon: ({focused}) =>
                    focused ? (
                      <UserIconSolid size={24} color="#091116" />
                    ) : (
                      <UserIcon size={24} color="#8a8d8f" />
                    ),
                }}
              />
            </Tab.Navigator>
          </NavigationContainer>
        </TailwindProvider>
      </ApolloProvider>
    </NotifierWrapper>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  logo: {
    width: 24,
    marginRight: 8,
  },
  headerLogo: {
    width: 24,
    height: 24,
  },
  headerText: {
    color: 'black',
    fontWeight: 'bold',
  },
});

export default App;
