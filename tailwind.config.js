module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        customTitle: ['PlayfairDisplay-Bold'],
      },
      colors: {
        choosePrimary: '#FEF8ED',
        chooseBlack: '#091116',
        chooseGrey: '#8a8d8f',
      },
    },
  },
  plugins: [],
  corePlugins: require('tailwind-rn/unsupported-core-plugins'),
};
